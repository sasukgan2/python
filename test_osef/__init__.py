#!/usr/bin/env python
# -*- coding: utf-8 -*-

__name__ = 'test_osef'
__version__ = '0.0.1.dev2'
__author__ = 'Morgan EBANDZA'
__author_email__ = 'morgan.ebandza@telecomnnancy.net'

__import__('pkg_resources').declare_namespace(__name__)
